FROM golang:alpine3.17

WORKDIR /app/test

COPY go.mod .

COPY go.sum .

# COPY .env .

RUN go mod download

COPY . .

# ENV PORT 8778

EXPOSE 3000

RUN go build

CMD ["./test"]
